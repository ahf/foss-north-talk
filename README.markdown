# FOSS North 2020 Slides

This repository contains the slides for the FOSS North 2020 presentation on Tor
named "Anonymity loves Diversity: The Case of Tor". The talk will be presented
by GeKo and ahf.

## Building the slides

1. Update the submodules to fetch the `onion-tex` dependency: `git submodule
   update --init --recursive`.

2. Run `make`.
